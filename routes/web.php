<?php

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/barcode', 'PDFController@index')->name('barcode');

Route::get('/', 'Auth\LoginController@login');
Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::get('/edituser', function () { return view('admin.edituser'); })->name('edituser');
Route::post('/login', 'Auth\LoginController@authenticate')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('/settings', 'SettingsController@index')->name('settings');

Route::post('/barcodescanner', 'AssetsController@barcodeScanning')->name('barcodescanner');

Route::group(['middleware' =>['permission:add asset category']],function(){
    Route::post('/settings/assetcategory', 'SettingsController@addAssetCategory')->name('settings.add.assettype');
});
Route::group(['middleware' =>['permission:add asset category']],function(){
    Route::post('/settings/deleteassetcategory', 'SettingsController@deleteAssetCategory')->name('settings.delete.assettype');
});
Route::group(['middleware' =>['permission:add department']],function(){
    Route::post('/settings/department', 'SettingsController@addDepartment')->name('settings.add.department');
});
Route::group(['middleware' =>['permission:delete department']],function(){
    Route::post('/settings/deletedepartment', 'SettingsController@deleteDepartment')->name('settings.delete.department');
});
Route::group(['middleware' =>['permission:add asset blueprint']],function(){
    Route::post('/settings/assetblueprint', 'SettingsController@addAssetBlueprint')->name('settings.add.assetblueprint');
});
Route::group(['middleware' =>['permission:delete asset blueprint']],function(){
    Route::post('/settings/deleteassetblueprint', 'SettingsController@deleteAssetBlueprint')->name('settings.delete.assetblueprint');
});
Route::group(['middleware' =>['permission:edit role permissions']],function(){
    Route::post('/settings/updateroleandpermission', 'SettingsController@updateRoleAndPermission')->name('settings.edit.rolepermission');
});
Route::group(['middleware' =>['permission:create role']],function(){
    Route::post('/settings/addrole', 'SettingsController@addRole')->name('settings.add.role');
});
Route::group(['middleware' =>['permission:create ppm checklist']],function(){
    Route::post('/settings/adddocument', 'SettingsController@addDocument')->name('settings.add.document');
});
Route::group(['middleware' =>['permission:create ppm checklist']],function(){
    Route::post('/settings/addquestion', 'SettingsController@addQuestion')->name('settings.add.question');
});
Route::group(['middleware' =>['permission:delete role']],function(){
    Route::post('/settings/deleterole', 'SettingsController@deleteRole')->name('settings.delete.role');
});
Route::group(['middleware' =>['permission:view asset']],function(){
    Route::get('/viewasset', 'AssetsController@index')->name('viewasset');
});
Route::group(['middleware' =>['permission:edit asset']],function(){
    Route::post('/updateasset', 'AssetsController@updateAsset')->name('updateasset');
});
Route::group(['middleware' =>['permission:edit asset']],function(){
    Route::post('/addpmschedule', 'AssetsController@addPmSchedule')->name('editasset.add.pmschedule');
});
Route::group(['middleware' =>['permission:create asset']],function(){
    Route::post('/viewasset/addasset', 'AssetsController@addAsset')->name('viewasset.add.asset');
});
Route::group(['middleware' =>['permission:create user']],function(){
    Route::get('/createuserpage', 'UserManagementController@createUserPage')->name('createuserpage');
    Route::post('/createuser', 'UserManagementController@createUser')->name('createuser');
});

Route::group(['middleware' =>['permission:edit user']],function(){
    Route::post('/edituserprofile', 'UserManagementController@editUser')->name('edituserprofile');
    Route::post('/edituserpermissions', 'UserManagementController@editUserPermissions')->name('edituserpermissions');
});
Route::group(['middleware' =>['permission:view user']],function(){
    Route::get('/viewuser', 'UserManagementController@index')->name('viewuser');
    Route::post('/edituserfromview', 'UserManagementController@editUserFromView')->name('edituserfromview');
});
Route::group(['middleware' =>['permission:change user status']],function(){
    Route::post('/changestatus', 'UserManagementController@changeStatus')->name('viewuser.changestatus');
});
Route::group(['middleware' =>['permission:edit asset']],function(){
    Route::post('/editasset', 'AssetsController@editAssetFromView')->name('editasset');
});
// Route::get('/editasset', function () { return view('admin.editasset'); })->name('editasset');
Route::group(['middleware' =>['permission:view reports']], function () {
    Route::get('/reports', function () { return view('admin.reports'); })->name('reports');
});
