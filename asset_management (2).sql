-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2022 at 10:02 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blueprint` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` bigint(20) NOT NULL,
  `expire_date` date NOT NULL,
  `assigned_user` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_department` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mac_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ppm_document` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets_blueprints`
--

CREATE TABLE `assets_blueprints` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `require_ppm` tinyint(1) DEFAULT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_histories`
--

CREATE TABLE `asset_histories` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assigned_user` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date DEFAULT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_pm_answers`
--

CREATE TABLE `asset_pm_answers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pm_schedule` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pm_question` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_pm_schedules`
--

CREATE TABLE `asset_pm_schedules` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pm_schedule_date` date NOT NULL,
  `done_at` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `done_by` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `asset_types`
--

CREATE TABLE `asset_types` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_09_17_034332_create_permission_tables', 1),
(5, '2021_12_05_131617_create_departments_table', 1),
(6, '2021_12_05_141755_create_asset_types_table', 1),
(7, '2021_12_11_154509_create_assets_blueprints_table', 1),
(8, '2021_12_11_163512_create_assets_table', 1),
(9, '2022_01_25_092725_create_pm_documents_table', 1),
(10, '2022_01_25_093303_create_pm_document_questions_table', 1),
(11, '2022_01_25_161453_create_asset_pm_schedules_table', 1),
(12, '2022_01_25_161654_create_asset_pm_answers_table', 1),
(13, '2022_01_25_161743_create_asset_histories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', '3d81a432-94b3-42d7-b778-92ef1f6fe8a6');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'create user', 'web', '2022-02-18 17:52:11', '2022-02-18 17:52:11'),
(2, 'view user', 'web', '2022-02-18 17:52:14', '2022-02-18 17:52:14'),
(3, 'edit user', 'web', '2022-02-18 17:52:22', '2022-02-18 17:52:22'),
(4, 'change user status', 'web', '2022-02-18 17:52:26', '2022-02-18 17:52:26'),
(5, 'create asset', 'web', '2022-02-18 17:52:28', '2022-02-18 17:52:28'),
(6, 'assign asset', 'web', '2022-02-18 17:52:33', '2022-02-18 17:52:33'),
(7, 'view asset', 'web', '2022-02-18 17:52:35', '2022-02-18 17:52:35'),
(8, 'edit asset', 'web', '2022-02-18 17:52:37', '2022-02-18 17:52:37'),
(9, 'create ppm checklist', 'web', '2022-02-18 17:52:39', '2022-02-18 17:52:39'),
(10, 'assign ppm asset', 'web', '2022-02-18 17:52:40', '2022-02-18 17:52:40'),
(11, 'create department', 'web', '2022-02-18 17:52:42', '2022-02-18 17:52:42'),
(12, 'edit department', 'web', '2022-02-18 17:52:43', '2022-02-18 17:52:43'),
(13, 'create role', 'web', '2022-02-18 17:52:45', '2022-02-18 17:52:45'),
(14, 'view role permissions', 'web', '2022-02-18 17:52:48', '2022-02-18 17:52:48'),
(15, 'edit role permissions', 'web', '2022-02-18 17:52:51', '2022-02-18 17:52:51'),
(16, 'delete role', 'web', '2022-02-18 17:52:51', '2022-02-18 17:52:51'),
(17, 'view reports', 'web', '2022-02-18 17:52:52', '2022-02-18 17:52:52'),
(18, 'view departments', 'web', '2022-02-18 17:52:53', '2022-02-18 17:52:53'),
(19, 'add department', 'web', '2022-02-18 17:52:54', '2022-02-18 17:52:54'),
(20, 'delete department', 'web', '2022-02-18 17:52:55', '2022-02-18 17:52:55'),
(21, 'view asset category', 'web', '2022-02-18 17:52:57', '2022-02-18 17:52:57'),
(22, 'add asset category', 'web', '2022-02-18 17:52:58', '2022-02-18 17:52:58'),
(23, 'delete asset category', 'web', '2022-02-18 17:53:19', '2022-02-18 17:53:19'),
(24, 'view asset blueprint', 'web', '2022-02-18 17:53:20', '2022-02-18 17:53:20'),
(25, 'add asset blueprint', 'web', '2022-02-18 17:53:21', '2022-02-18 17:53:21'),
(26, 'delete asset blueprint', 'web', '2022-02-18 17:53:22', '2022-02-18 17:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `pm_documents`
--

CREATE TABLE `pm_documents` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pm_document_questions`
--

CREATE TABLE `pm_document_questions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pm_document` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2022-02-18 17:51:55', '2022-02-18 17:51:55'),
(2, 'procurement officer (PMUo)', 'web', '2022-02-18 17:51:56', '2022-02-18 17:51:56'),
(3, 'head of department (HoD)', 'web', '2022-02-18 17:52:01', '2022-02-18 17:52:01'),
(4, 'normal user', 'web', '2022-02-18 17:52:03', '2022-02-18 17:52:03'),
(5, 'technical officer (To)', 'web', '2022-02-18 17:52:05', '2022-02-18 17:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(8, 1),
(8, 2),
(9, 1),
(10, 1),
(10, 5),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_names` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `current_department` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profilepath` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_names`, `last_name`, `email`, `phone_number`, `email_verified_at`, `password`, `is_active`, `current_department`, `profilepath`, `remember_token`, `created_at`, `updated_at`) VALUES
('3d81a432-94b3-42d7-b778-92ef1f6fe8a6', 'patrick.augustino', 'Patrick', 'Augustino', 'admin@jkci.co.tz', '0659509332', NULL, '$2y$10$UUiCTNcQEmm7pHvw/ruOheLE32Cga.lPPlUBK1BblYpEH2QhEXrAW', 1, 'ICT', NULL, NULL, '2022-02-18 17:51:53', '2022-02-18 17:51:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assets_barcode_unique` (`barcode`),
  ADD UNIQUE KEY `assets_mac_address_unique` (`mac_address`),
  ADD KEY `assets_created_by_foreign` (`created_by`),
  ADD KEY `assets_assigned_user_foreign` (`assigned_user`),
  ADD KEY `assets_current_department_foreign` (`current_department`),
  ADD KEY `assets_blueprint_foreign` (`blueprint`);

--
-- Indexes for table `assets_blueprints`
--
ALTER TABLE `assets_blueprints`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assets_blueprints_image_unique` (`image`),
  ADD KEY `assets_blueprints_created_by_foreign` (`created_by`),
  ADD KEY `assets_blueprints_type_foreign` (`type`);

--
-- Indexes for table `asset_histories`
--
ALTER TABLE `asset_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_histories_created_by_foreign` (`created_by`),
  ADD KEY `asset_histories_asset_foreign` (`asset`),
  ADD KEY `asset_histories_assigned_user_foreign` (`assigned_user`),
  ADD KEY `asset_histories_department_foreign` (`department`);

--
-- Indexes for table `asset_pm_answers`
--
ALTER TABLE `asset_pm_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_pm_answers_pm_schedule_foreign` (`pm_schedule`),
  ADD KEY `asset_pm_answers_pm_question_foreign` (`pm_question`);

--
-- Indexes for table `asset_pm_schedules`
--
ALTER TABLE `asset_pm_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_pm_schedules_created_by_foreign` (`created_by`),
  ADD KEY `asset_pm_schedules_done_by_foreign` (`done_by`),
  ADD KEY `asset_pm_schedules_approved_by_foreign` (`approved_by`),
  ADD KEY `asset_pm_schedules_asset_foreign` (`asset`);

--
-- Indexes for table `asset_types`
--
ALTER TABLE `asset_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `asset_types_details_unique` (`details`),
  ADD KEY `asset_types_created_by_foreign` (`created_by`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_department_name_unique` (`department_name`),
  ADD KEY `departments_created_by_foreign` (`created_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `pm_documents`
--
ALTER TABLE `pm_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pm_documents_created_by_foreign` (`created_by`);

--
-- Indexes for table `pm_document_questions`
--
ALTER TABLE `pm_document_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pm_document_questions_created_by_foreign` (`created_by`),
  ADD KEY `pm_document_questions_pm_document_foreign` (`pm_document`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_name_unique` (`user_name`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_profilepath_unique` (`profilepath`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_assigned_user_foreign` FOREIGN KEY (`assigned_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `assets_blueprint_foreign` FOREIGN KEY (`blueprint`) REFERENCES `assets_blueprints` (`id`),
  ADD CONSTRAINT `assets_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `assets_current_department_foreign` FOREIGN KEY (`current_department`) REFERENCES `departments` (`id`);

--
-- Constraints for table `assets_blueprints`
--
ALTER TABLE `assets_blueprints`
  ADD CONSTRAINT `assets_blueprints_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `assets_blueprints_type_foreign` FOREIGN KEY (`type`) REFERENCES `asset_types` (`id`);

--
-- Constraints for table `asset_histories`
--
ALTER TABLE `asset_histories`
  ADD CONSTRAINT `asset_histories_asset_foreign` FOREIGN KEY (`asset`) REFERENCES `assets` (`id`),
  ADD CONSTRAINT `asset_histories_assigned_user_foreign` FOREIGN KEY (`assigned_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `asset_histories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `asset_histories_department_foreign` FOREIGN KEY (`department`) REFERENCES `departments` (`id`);

--
-- Constraints for table `asset_pm_answers`
--
ALTER TABLE `asset_pm_answers`
  ADD CONSTRAINT `asset_pm_answers_pm_question_foreign` FOREIGN KEY (`pm_question`) REFERENCES `pm_document_questions` (`id`),
  ADD CONSTRAINT `asset_pm_answers_pm_schedule_foreign` FOREIGN KEY (`pm_schedule`) REFERENCES `asset_pm_schedules` (`id`);

--
-- Constraints for table `asset_pm_schedules`
--
ALTER TABLE `asset_pm_schedules`
  ADD CONSTRAINT `asset_pm_schedules_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `asset_pm_schedules_asset_foreign` FOREIGN KEY (`asset`) REFERENCES `assets` (`id`),
  ADD CONSTRAINT `asset_pm_schedules_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `asset_pm_schedules_done_by_foreign` FOREIGN KEY (`done_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `asset_types`
--
ALTER TABLE `asset_types`
  ADD CONSTRAINT `asset_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `pm_documents`
--
ALTER TABLE `pm_documents`
  ADD CONSTRAINT `pm_documents_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `pm_document_questions`
--
ALTER TABLE `pm_document_questions`
  ADD CONSTRAINT `pm_document_questions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pm_document_questions_pm_document_foreign` FOREIGN KEY (`pm_document`) REFERENCES `pm_documents` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
