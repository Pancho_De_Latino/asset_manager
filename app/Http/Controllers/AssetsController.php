<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\AssetHistory;
use App\Models\AssetPmSchedule;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\AssetType;
use App\Models\AssetsBlueprint;
use App\Models\PmDocument;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class AssetsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(){
        $assetcategories = AssetType::all();

        $departments = Department::all();

        $assetblueprints = DB::table('assets_blueprints')
            ->select('*', DB::raw('assets_blueprints.id as blueprint_id'))
            ->leftJoin('asset_types','type',"=","asset_types.id")
            ->get();

        $assets = DB::table('assets')
            ->select('*', DB::raw('assets.id as assets_id',))
            ->leftJoin('assets_blueprints','assets.blueprint',"=","assets_blueprints.id")
            ->leftJoin('asset_types','assets_blueprints.type',"=","asset_types.id")
            ->leftJoin('departments','assets.current_department',"=","departments.id")
            ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
            ->get();

        return view('admin.viewasset', [
            'assetcategories' => $assetcategories,
            'departments' => $departments,
            'assetblueprints' => $assetblueprints,
            'assets' => $assets,
        ]);
    }

    function generateBarcodeNumber() {
        $number = mt_rand(1000000000, 9999999999);
        while(DB::table('assets')->where('barcode', $number)->exists()){
            $number = mt_rand(1000000000, 9999999999);
        }
        return $number;
    }
    function addAsset(Request $request){
        $this->validate($request,[
            'blueprint'=>'required',
            'expire_date'=>'required|after_or_equal:today',
            'current_department'=>'required',
            'serial_number'=>'required|alpha_dash',
            'mac_address'=>'required|alpha_dash',
        ]);
        try{
            $asset = new Asset();
            $asset->blueprint = $request->blueprint;
            $asset->expire_date = $request->expire_date;
            $asset->serial_number = $request->serial_number;
            $asset->current_department = $request->current_department;
            $asset->barcode = $this->generateBarcodeNumber();
            $asset->status = "created";

            if ($request->has('mac_address')) {
                $asset->mac_address = $request->mac_address;
            }
            $asset->created_by = Auth::user()->id;
            $asset->save();

            $assethistory=new AssetHistory();
            $assethistory->department = $request->current_department;
            $assethistory->asset = $asset->id;
            $assethistory->fromdate = Carbon::now();
            $assethistory->created_by = Auth::user()->id;
            $assethistory->save();
            return redirect()->back()->with('barcode', $asset->barcode);
        }catch (Exception $e){
           return redirect()->back()->with('error', 'Asset Blueprint was not Saved');
        }
    }
    function updateAsset(Request $request){
        $asset = Asset::find($request->asset_id);
        if($asset->current_department!=$request->current_department | $asset->assigned_user!=$request->assigned_to){
            $assethistory= AssetHistory::orderByDesc('fromdate')
                ->where('asset','=',$asset->id)
                ->first();
            $assethistory->todate=Carbon::now();
            $assethistory->save();

            $newassetHistory=new AssetHistory();
            $newassetHistory->department = $request->current_department;
            $newassetHistory->asset = $asset->id;
            $newassetHistory->assigned_user = $request->assigned_to != null ? $request->assigned_to : null;
            $newassetHistory->fromdate = Carbon::now();
            $newassetHistory->created_by = Auth::user()->id;
            $newassetHistory->save();
        }
        $asset->blueprint = $request->blueprint;
        $asset->expire_date = $request->expire_date;
        $asset->serial_number = $request->serial_number;
        $asset->current_department = $request->current_department;
        $asset->status = $request->status;
        $asset->assigned_user = $request->assigned_to != null ? $request->assigned_to : null;
        $asset->mac_address = $request->mac_address != null ? $request->mac_address : $asset->mac_address;
        // return $request->all();
        try{
            $asset->save();
            $asset_id = $request->asset_id;
            return App::call(
                'App\Http\Controllers\AssetsController@editAssetPage', ['asset_id' => $asset_id]
            );
        }catch(Exception $e){
            $asset_id = $request->asset_id;
            return App::call(
                'App\Http\Controllers\AssetsController@editAssetPage', ['asset_id' => $asset_id]
            );
        }
    }
    function addPmSchedule(Request $request){
        // return $request->all();
        $asset_pm_schedule = new AssetPmSchedule();
        $asset_pm_schedule->pm_schedule_date=$request->pm_schedule_date;
        $asset_pm_schedule->asset = $request->asset_id;
        $asset_pm_schedule->created_by = Auth::user()->id;
        $asset_pm_schedule->save();

        $asset_id = $request->asset_id;

        $asset=Asset::find($asset_id);
        $asset->ppm_document=$request->ppm_document;
        $asset->save();

        return App::call(
            'App\Http\Controllers\AssetsController@editAssetPage', ['asset_id' => $asset_id]
        );
    }
    function editAssetPage($asset_id){
        $assetdetails = Asset::with(['pm_schedule','pm_document','department','asset_blueprint','asset_history'])
                        ->where('id','=',$asset_id)
                        ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
                        ->first();
        // return $assetdetails;
        $departments = Department::all();

        $assetblueprints = DB::table('assets_blueprints')
            ->select('*', DB::raw('assets_blueprints.id as blueprint_id'))
            ->leftJoin('asset_types','type',"=","asset_types.id")
            ->get();

        $users = DB::table('users')
            ->select('*')
            ->where('is_active','=',true)
            ->get();

        $pm_documents = PmDocument::all();

        $asset = DB::table('assets')
            ->select('*', DB::raw('assets.id as assets_id',))
            ->leftJoin('assets_blueprints','assets.blueprint',"=","assets_blueprints.id")
            ->leftJoin('asset_types','assets_blueprints.type',"=","asset_types.id")
            ->leftJoin('departments','assets.current_department',"=","departments.id")
            ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
            ->where('assets.id','=',$asset_id)
            ->get();
        return view('admin.editasset',[
            'asset'=>$assetdetails,
            'departments'=>$departments,
            'assetblueprints'=>$assetblueprints,
            'users'=>$users,
            'pm_documents' => $pm_documents
        ]);
    }
    function editAssetFromView(Request $request){
        $asset_id = $request->asset_id;
        return App::call(
            'App\Http\Controllers\AssetsController@editAssetPage', ['asset_id' => $asset_id]
        );
    }
    function barcodeScanning(Request $request){
        $this->validate($request,[
            'barcode'=>'required',
        ]);
        // return $request->all();
        $barcode = substr($request->barcode,0,10);
        $asset = Asset::where('barcode','=',$barcode)->first();
        if(!empty($asset)){
            $asset_id= $asset->id;

            return App::call(
                'App\Http\Controllers\AssetsController@editAssetPage', ['asset_id' => $asset_id]
            );
        } else {
            return redirect()->back();
        }

    }


}
