<?php

namespace App\Http\Controllers\ApiControllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $user_name = $request->user_name;
        $password = $request->password;
        if (Auth::attempt(['user_name' => $user_name, 'password' => $password, 'is_active' => 1])) {

            return response()->json([
                "id" => "".Auth::user()->id."",
                "user_name" => "".Auth::user()->user_name."",
                "first_names" => "".Auth::user()->first_names."",
                "last_name" => "".Auth::user()->last_name."",
                "email" => "".Auth::user()->email."",
                "phone_number" => "".Auth::user()->phone_number."",
                "profilepath" => "".Auth::user()->profilepath."",
            ], 200);

            //return redirect()->route('welcome');
        } else {
            return response()->json([
                "error" => "Incorrect Credentials",
            ], 400);
        }

    }
}
