<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(Request $request){
        // return $request->all();
        return $pdf = PDF::loadView('barcode',['barcode' => $request->barcode])->setPaper('a6','landscape')->setWarnings(false);
        // return $pdf->download('barcode.pdf');
    }
}
