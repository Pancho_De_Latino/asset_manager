<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Department;
use App\User;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserManagementController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(){
        $users = DB::table('users')
            ->select('*', DB::raw('users.id as user_id','roles.id as role_id',))
            ->leftJoin('model_has_roles','model_has_roles.model_id',"=","users.id")
            ->leftJoin('departments','departments.id',"=","users.current_department")
            ->leftJoin('roles','roles.id',"=","model_has_roles.role_id")
            ->get();

        return view('admin.viewuser', [
            'users' => $users,
        ]);
    }
    function createUserPage(){
        $departments = Department::all();

        $assets = DB::table('assets')
            ->select('*', DB::raw('assets.id as assets_id',))
            ->leftJoin('assets_blueprints','assets.blueprint',"=","assets_blueprints.id")
            ->leftJoin('asset_types','assets_blueprints.type',"=","asset_types.id")
            ->leftJoin('departments','assets.current_department',"=","departments.id")
            ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
            ->get();

        $roles = Role::all();

        return view('admin.createuser', [
            'assets' => $assets,
            'departments' => $departments,
            'roles' => $roles
        ]);
    }
    function createUser(Request $request){
        //return $request->all();
        $this->validate($request,[
            'user_name'=>'required',
            'first_names'=>'required|alpha',
            'last_name'=>'required|alpha',
            'phone_number'=>'required|digits_between:10,12',
            'email'=>'required|email|regex:/^.+@.+$/i',
            'current_department'=>'required',
            'role'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        try{
            $path = $request->image->store('public');
            $user = new User();
            $user->user_name = $request->user_name;
            $user->first_names = $request->first_names;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->current_department = $request->current_department;
            $user->profilepath = $path;
            $user->password = bcrypt('12345');
            $user->is_active = true;
            $user->save();

            $role = $request->role;
            $user->assignRole($role);
            $userid = $user->id;
            return App::call(
                'App\Http\Controllers\UserManagementController@editUserPage', ['userid' => $userid]
            );
        //     return redirect()->back()->with('success', 'User was Successfully Created');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'user was not Created');
        }
    }
    function editUserPage($userid){

        $assignedssets = DB::table('assets')
            ->select('*', DB::raw('assets.id as assets_id',))
            ->leftJoin('assets_blueprints','assets.blueprint',"=","assets_blueprints.id")
            ->leftJoin('asset_types','assets_blueprints.type',"=","asset_types.id")
            ->leftJoin('departments','assets.current_department',"=","departments.id")
            ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
            ->whereNotNull ('assigned_user')
            ->where('assigned_user', '=', $userid)
            ->get();

        $departments = Department::all();

        $roles = Role::all();

        $permission = Permission::all();



        $userrolepermission = DB::table('permissions')
            ->select('*', DB::raw('permissions.id as permission_id','model.id as user_id'))
            ->leftJoin('role_has_permissions','role_has_permissions.permission_id',"=","permissions.id")
            ->leftJoin('roles','roles.id',"=","role_has_permissions.role_id")
            ->leftJoin('model_has_roles','roles.id',"=","model_has_roles.role_id")
            ->where('model_has_roles.model_id', '=', $userid)
            ->get();

        $userpermission = DB::table('permissions')
            ->select('permissions.*', DB::raw('permissions.id as permission_id','model.id as user_id'))
            ->leftJoin('model_has_permissions','model_has_permissions.permission_id',"=","permissions.id")
            ->whereNotNull('model_id')
            ->where('model_id', '==', $userid)
            // ->union($userrolepermission)
            ->get();

        $unassignedassets = DB::table('assets')
            ->select('*', DB::raw('assets.id as assets_id',))
            ->leftJoin('assets_blueprints','assets.blueprint',"=","assets_blueprints.id")
            ->leftJoin('asset_types','assets_blueprints.type',"=","asset_types.id")
            ->leftJoin('departments','assets.current_department',"=","departments.id")
            ->whereNotIn('assets.status', ['disabled', 'expired', 'dead'])
            ->whereNull('assigned_user')
            ->orWhere('assigned_user', '!=', $userid)
            ->get();

        $users = DB::table('users')
            ->select('*', DB::raw('users.id as user_id','roles.id as role_id',))
            ->leftJoin('model_has_roles','model_has_roles.model_id',"=","users.id")
            ->leftJoin('departments','departments.id',"=","users.current_department")
            ->leftJoin('roles','roles.id',"=","model_has_roles.role_id")
            ->where('users.id', '=', $userid)
            ->limit(1)
            ->get();

        $user = User::find($userid);
        // return $user->getAllPermissions();

        return view('admin.edituser', [
            'user' => $users,
            'assignedssets' => $assignedssets,
            'unassignedassets' => $unassignedassets,
            'departments' => $departments,
            'roles' => $roles,
            'permissions' => $permission,
            'userrolepermission' => $user->getAllPermissions(),
            'userpermissionsviaroles' => $user->getPermissionsViaRoles(),

        ]);
    }

    function editUser(Request $request){

        $userid = $request->user_id;

        $user = User::find($userid);
        if(is_null($request->image)){
            $profilepath=$user->profilepath;
        } else {
            $profilepath = $request->image->store('public');
        }

        $user = User::find($userid);
        $user->first_names=$request->first_names;
        $user->last_name=$request->last_name;
        $user->phone_number=$request->phone_number;
        $user->email=$request->email;
        $user->current_department=$request->current_department;
        $user->profilepath=$profilepath;

        $user->save();

        $role = $request->role;
        $user->syncRoles($role);

        return App::call(
            'App\Http\Controllers\UserManagementController@editUserPage', ['userid' => $userid]
        );
    }
    function editUserFromView(Request $request){
        $userid = $request->user_id;
        return App::call(
            'App\Http\Controllers\UserManagementController@editUserPage', ['userid' => $userid]
        );
    }

    function changeStatus(Request $request){
        $user_id = $request->user_id;

        $user = User::find($user_id);
        $user->is_active = $user->is_active ? false : true;
        $user->save();
        return redirect()->back()->with('success', 'User Status Changed Saved');

    }
    function editUserPermissions(Request $request){
        // return $request->all();
        $userid = $request->user_id;
        $user = User::find($userid);

        $permissions = array();


        foreach($request->all() as $key => $value){
            if(is_int($key)){
                $permissions[]=Permission::findById($key);
            }
        }
        $user->syncPermissions($permissions);
        return App::call(
            'App\Http\Controllers\UserManagementController@editUserPage', ['userid' => $userid]
        );
    }

}
