<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\AssetType;
use App\Models\AssetsBlueprint;
use App\Models\PmDocument;
use App\Models\PmDocumentQuestion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SettingsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    function index(){
        $assetcategories = AssetType::all();

        $departments = Department::all();

        $permissions = Permission::all();

        $roles = Role::all();

        $documents = PmDocument::with('pm_questions')->get();

        $assetblueprints = DB::table('assets_blueprints')
            ->select('*', DB::raw('assets_blueprints.id as blueprint_id'))
            ->leftJoin('asset_types','type',"=","asset_types.id")
            ->get();

        // return AssetsBlueprint::with([
        //     'asset_typ' => function($quey){
        //         $quey->where('id','like','%');
        //     },
        //      'created_with'])
        //     ->get();

        return view('admin.settings', [
            'assetcategories' => $assetcategories,
            'departments' => $departments,
            'assetblueprints' => $assetblueprints,
            'permissions' => $permissions,
            'roles' => $roles,
            'documents'=>$documents,
        ]);
    }
    function addAssetCategory(Request $request){
        $this->validate($request,[
            'details'=>['required']
        ]);
        try{
            $assetcategory = new AssetType;
            $assetcategory->details = $request->details;
            $assetcategory->created_by = Auth::user()->id;
            $assetcategory->save();
            return redirect()->back()->with('success', 'Asset Category Has Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'Asset Category was not Saved');
        }
    }
    function addDepartment(Request $request){
        $this->validate($request,[
            'department_name'=>['required']
        ]);
        try{
            $department = new Department();
            $department->department_name = $request->department_name;
            $department->created_by = Auth::user()->id;
            $department->save();
            return redirect()->back()->with('success', 'Department Has Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'Department was not Saved');
        }
    }
    function addAssetBlueprint(Request $request){
        // return $request->all();
        $this->validate($request,[
            'name'=>['required'],
            'model'=>['required'],
            'type'=>['required'],
            'image'=>['required'],
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $request->flash();
        try{
            $path = $request->image->store('public');
            $assetblueprint = new AssetsBlueprint();
            $assetblueprint->name = $request->name;
            $assetblueprint->type = $request->type;
            $assetblueprint->model = $request->model;
            $assetblueprint->image = $path;
            if ($request->has('require_ppm')) {
                $assetblueprint->require_ppm = true;
            }
            $assetblueprint->created_by = Auth::user()->id;
            $assetblueprint->save();
            return redirect()->back()->with('success', 'Asset BluePrint was Successfully Saved');
        }catch (Exception $e){
            return redirect()->back()->with('error', 'Asset Blueprint was not Saved');
        }
    }

    function deleteAssetBlueprint(Request $request){
        $blueprint_id = $request->blueprint_id;
        try{
            AssetsBlueprint::where('id', $blueprint_id)->delete();
            return redirect()->back()->with('success', 'Asset BluePrint was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Asset Blueprint was not Deleted');
        }
    }

    function deleteAssetCategory(Request $request){
        $id = $request->assetcategory_id;
        try{
            AssetType::where('id', $id)->delete();
            return redirect()->back()->with('success', 'Asset Category was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Asset Category was not Deleted');
        }
    }
    function deleteDepartment(Request $request){
        $id = $request->department_id;
        try{
            Department::where('id', $id)->delete();
            return redirect()->back()->with('success', 'Department was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Department was not Deleted');
        }
    }
    function updateRoleAndPermission(Request $request){
        // return $request->all();
        try{
            $role = array();
            foreach($request->all() as $key=>$value){
                if(str_contains($key,'-')){
                    $role[] = substr($key, 0,strpos($key, "-"));
                }

            }
            // return $role;
            $roles = array_unique($role);
            $permissions = array();
            foreach($roles as $keyrole=>$valuerole){
                $permissions = array();
                foreach($request->all() as $key=>$value){
                    $rolefetched = array();
                    if(str_contains($key,'-')){
                        $rolefetched[] = substr($key, 0,strpos($key, "-"));

                        if($valuerole == end($rolefetched)){
                            $permissions[] = substr($key, strpos($key, "-")+1,strlen($key));
                        }
                    }
                }
                $theRole = Role::find($valuerole);
                $theRole->syncPermissions($permissions);
            }

            return redirect()->back()->with('success', 'Roles were successfully Updated');
        } catch(Exception $e){
            return redirect()->back()->with('error', 'Roles were not Updated, Assigned Roles are Restricted');
        }




        // foreach($request->all() as $key => $value){
        //     $role[] = substr($key, strpos($key, "_") + 1);
        //     if(is_int($key)){
        //         $permissions[]=Permission::findById($key);
        //     }
        // }
        // $roles = array_unique($role);
        // $permissions->syncPermissions($permissions);


        // $data = "123_String";
        // $whatIWant = substr($data, strpos($data, "_") + 1);
        // echo $whatIWant;
    }
    function addRole(Request $request){
        // return $request->all();

        $role = $request->role_name;

        try{
            Role::create(['name' => $role]);
            return redirect()->back()->with('success', 'Role was Successfully Created');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Role was not Created');
        }

    }
    function addDocument(Request $request){
        // return $request->all();

        $document_name = $request->document_name;

        try{
            $document = new PmDocument();
            $document->name=$document_name;
            $document->created_by=Auth::user()->id;
            $document->save();
            return redirect()->back()->with('success', 'Document was Successfully Created');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Document was not Created');
        }

    }
    function addQuestion(Request $request){
        // return $request->all();

        $document_id = $request->document_id;
        $document_question = $request->document_question;

        try{
            $documentQuestion = new PmDocumentQuestion();
            $documentQuestion->name=$document_question;
            $documentQuestion->pm_document = $document_id;
            $documentQuestion->created_by=Auth::user()->id;
            $documentQuestion->save();
            return redirect()->back()->with('success', 'Document Checklist Successfully Added');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Document Checklist was not Added');
        }

    }
    function deleteRole(Request $request){
        $id = $request->role_id;
        try{
            Role::where('id', $id)->delete();
            return redirect()->back()->with('success', 'Role was Successfully Deleted');
        }catch(Exception $e){
            return redirect()->back()->with('error', 'Role was not Deleted');
        }
    }

}
