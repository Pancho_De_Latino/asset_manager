<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class PmDocument extends Model
{
    use Uuids;
    public function pm_questions(){
        return $this->hasMany(PmDocumentQuestion::class,'pm_document');
    }
}
