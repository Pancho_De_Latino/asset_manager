<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\User;

class AssetsBlueprint extends Model
{
    use Uuids;
    protected $fillable = ['name','model','imagename','type'];

    public function asset_typ(){
        return $this->belongsTo(AssetType::class, 'type');
    }

    public function created_with(){
        return $this->belongsTo(User::class, 'created_by');
    }
}
