<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class AssetType extends Model
{
    use Uuids;
    protected $fillable = ['details'];

}
