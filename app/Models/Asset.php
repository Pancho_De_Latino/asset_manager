<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Asset extends Model
{
    use Uuids;
    protected $fillable = ['blueprint','barcode','serial_number','mac_address'];

    public function asset_blueprint(){
        return $this->belongsTo(AssetsBlueprint::class, 'blueprint');
    }
    public function department(){
        return $this->belongsTo(Department::class, 'current_department');
    }
    public function asset_history(){
        return $this->hasMany(AssetHistory::class, 'asset');
    }
    public function pm_document(){
        return $this->belongsTo(PmDocument::class, 'ppm_document');
    }
    public function pm_schedule(){
        return $this->hasMany(AssetPmSchedule::class, 'asset');
    }
}
