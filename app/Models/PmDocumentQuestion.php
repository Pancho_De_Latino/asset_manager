<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class PmDocumentQuestion extends Model
{
    use Uuids;
    public function pm_document(){
        return $this->belongsTo(PmDocument::class,'pm_document');
    }
}
