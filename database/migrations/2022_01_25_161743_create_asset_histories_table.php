<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_histories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('assigned_user')->nullable();
            $table->uuid('department');
            $table->uuid('asset');
            $table->date('fromdate');
            $table->date('todate')->nullable();
            $table->uuid('created_by');
            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
            $table->foreign('asset')
                ->references('id')
                ->on('assets')
                ->onDelete('restrict');
            $table->foreign('assigned_user')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
            $table->foreign('department')
                ->references('id')
                ->on('departments')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_histories');
    }
}
