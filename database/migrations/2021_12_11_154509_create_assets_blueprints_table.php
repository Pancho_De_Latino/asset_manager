<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsBlueprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_blueprints', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('model');
            $table->uuid('type');
            $table->string('image')->unique();
            $table->boolean('require_ppm')->nullable();
            $table->uuid('created_by');
            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('type')
                ->references('id')
                ->on('asset_types')
                ->onDelete('restrict');

            // $table->unique(array('name', 'model', 'type'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_blueprints');
    }
}
