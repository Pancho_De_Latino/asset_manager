<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetPmSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_pm_schedules', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('asset');
            $table->date('pm_schedule_date');
            $table->date('done_at')->nullable();
            $table->boolean('status')->default(false);
            $table->uuid('done_by')->nullable();
            $table->uuid('approved_by')->nullable();
            $table->uuid('created_by');
            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('done_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('approved_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('asset')
                ->references('id')
                ->on('assets')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_pm_schedules');
    }
}
