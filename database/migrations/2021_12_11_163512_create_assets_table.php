<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('blueprint');
            $table->uuid('created_by');
            $table->bigInteger('barcode')->unique();
            $table->date('expire_date');
            $table->uuid('assigned_user')->nullable();
            $table->uuid('current_department');
            $table->string('status');
            $table->string('mac_address')->nullable()->unique();
            $table->string('serial_number');
            $table->uuid('ppm_document')->nullable();//PPM DOCUMENT NOT YET
            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('assigned_user')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('current_department')
                ->references('id')
                ->on('departments')
                ->onDelete('restrict');

            $table->foreign('blueprint')
                ->references('id')
                ->on('assets_blueprints')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
