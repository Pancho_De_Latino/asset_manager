<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetPmAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_pm_answers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('pm_schedule');
            $table->uuid('pm_question');
            $table->boolean('answer')->default(false);
            $table->timestamps();

            $table->foreign('pm_schedule')
                ->references('id')
                ->on('asset_pm_schedules')
                ->onDelete('restrict');

            $table->foreign('pm_question')
                ->references('id')
                ->on('pm_document_questions')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_pm_answers');
    }
}
