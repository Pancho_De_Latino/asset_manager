<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $adminUser = User::create([
            'user_name' => 'patrick.augustino',
            'first_names' => 'Patrick',
            'last_name' => 'Augustino',
            'email' => 'admin@jkci.co.tz',
            'password' => bcrypt('12345'),
            'phone_number' => '0659509332',
            'current_department' => 'ICT',
            'is_active' => true,
        ]);
        $admin = Role::create(['name' => 'admin']);
        $pmuo = Role::create(['name' => 'procurement officer (PMUo)']);
        $hod = Role::create(['name' => 'head of department (HoD)']);
        $normal = Role::create(['name' => 'normal user']);
        $to = Role::create(['name' => 'technical officer (To)']);

        $createuser = Permission::create(['name' => 'create user']);
        $viewuser = Permission::create(['name' => 'view user']);
        $edituser = Permission::create(['name' => 'edit user']);
        $changeuserstatus = Permission::create(['name' => 'change user status']);
        $createasset = Permission::create(['name' => 'create asset']);
        $assignasset = Permission::create(['name' => 'assign asset']);
        $viewasset = Permission::create(['name' => 'view asset']);
        $editasset = Permission::create(['name' => 'edit asset']);
        $createppm = Permission::create(['name' => 'create ppm checklist']);
        $assignppm = Permission::create(['name' => 'assign ppm asset']);
        $createdepartment = Permission::create(['name' => 'create department']);
        $editdepartment = Permission::create(['name' => 'edit department']);
        $createrole = Permission::create(['name' => 'create role']);
        $viewrolepermission = Permission::create(['name' => 'view role permissions']);
        $editrole = Permission::create(['name' => 'edit role permissions']);
        $deleterole = Permission::create(['name' => 'delete role']);
        $viewreport = Permission::create(['name' => 'view reports']);
        $viewdepartment = Permission::create(['name' => 'view departments']);
        $adddepartment = Permission::create(['name' => 'add department']);
        $deletedepartment = Permission::create(['name' => 'delete department']);
        $viewassetcategory = Permission::create(['name' => 'view asset category']);
        $addassetcategory = Permission::create(['name' => 'add asset category']);
        $deleteassetcategory = Permission::create(['name' => 'delete asset category']);
        $viewassetblueprint = Permission::create(['name' => 'view asset blueprint']);
        $addassetblueprint = Permission::create(['name' => 'add asset blueprint']);
        $deleteassetblueprint = Permission::create(['name' => 'delete asset blueprint']);

        $admin->givePermissionTo($createuser);
        $admin->givePermissionTo($viewuser);
        $admin->givePermissionTo($edituser);
        $admin->givePermissionTo($createasset);
        $admin->givePermissionTo($assignasset);
        $admin->givePermissionTo($viewasset);
        $admin->givePermissionTo($editasset);
        $admin->givePermissionTo($createppm);
        $admin->givePermissionTo($changeuserstatus);
        $admin->givePermissionTo($assignppm);
        $admin->givePermissionTo($createdepartment);
        $admin->givePermissionTo($editdepartment);
        $admin->givePermissionTo($createrole);
        $admin->givePermissionTo($viewrolepermission);
        $admin->givePermissionTo($editrole);
        $admin->givePermissionTo($deleterole);
        $admin->givePermissionTo($viewreport);
        $admin->givePermissionTo($viewdepartment);
        $admin->givePermissionTo($adddepartment);
        $admin->givePermissionTo($deletedepartment);
        $admin->givePermissionTo($viewassetcategory);
        $admin->givePermissionTo($addassetcategory);
        $admin->givePermissionTo($deleteassetcategory);
        $admin->givePermissionTo($viewassetblueprint);
        $admin->givePermissionTo($addassetblueprint);
        $admin->givePermissionTo($deleteassetblueprint);

        $pmuo->givePermissionTo($viewuser);
        $pmuo->givePermissionTo($createasset);
        $pmuo->givePermissionTo($assignasset);
        $pmuo->givePermissionTo($viewasset);
        $pmuo->givePermissionTo($editasset);
        $pmuo->givePermissionTo($viewreport);

        $hod->givePermissionTo($viewuser);
        $hod->givePermissionTo($assignasset);
        $hod->givePermissionTo($viewasset);
        $hod->givePermissionTo($viewreport);

        $normal->givePermissionTo($viewasset);
        $normal->givePermissionTo($viewreport);

        $to->givePermissionTo($viewasset);
        $to->givePermissionTo($assignppm);
        $to->givePermissionTo($viewreport);

        $adminUser->assignRole($admin);

    }
}
