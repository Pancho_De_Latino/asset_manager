@extends('layouts.admin')
@section('title', 'Create Asset')
@section('admin-content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Create Asset</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Create Asset</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Asset BluePrint</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form>
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Asset Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter asset name">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Model</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter asset model">
                </div>
                <div class="form-group">
                    <label>Asset Type</label>
                    <select class="form-control select2bs4" style="width: 100%;">
                      <option></option>
                      <option>Furniture</option>
                      <option selected>Electonics</option>
                    </select>
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Requires Preventive Maintanance</label>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- general form elements disabled -->
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">Unique Asset Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form>
                  <!-- Date -->
                <div class="form-group">
                    <label>Date:</label>
                    <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Enter Expire Date">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">MAC Address</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter asset MAC address">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Serial Number</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter asset Serial Number">
                  </div>
                  <div class="form-group">
                    <label>Department</label>
                    <select class="form-control select2bs4" style="width: 100%;">
                      <option></option>
                      <option>Procurement Management Unit</option>
                      <option>Information Technology</option>
                      <option>Human Resource</option>
                      <option>Accounts and Finance</option>
                      <option>Technical Support</option>
                      <option>Legal Affairs</option>
                    </select>
                </div>
                  <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
              </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
