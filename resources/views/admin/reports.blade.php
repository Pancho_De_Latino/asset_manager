@extends('layouts.admin')
@section('title', 'Reports')
@section('admin-content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Reports</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Reports</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="card card-primary card-outline card-tabs">
            <div class="card-header p-0 pt-1 border-bottom-0">
              <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Disposed Assets</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Preventive Maintenance</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">User Audit Report</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-tabs-three-settings-tab" data-toggle="pill" href="#custom-tabs-three-settings" role="tab" aria-controls="custom-tabs-three-settings" aria-selected="false">Asset Movement</a>
                </li>
              </ul>
            </div>
            <div class="card-body">
              <div class="tab-content" id="custom-tabs-three-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <!-- Main content -->
                    <div class="row">
                            <div class="col-12">

                            <div class="card">
                                <div class="card-header">
                                <h3 class="card-title">DataTable with All Users</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                    <th>User ID</th>
                                    <th>Full Names</th>
                                    <th>Phone number</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Department</th>
                                    <th>Role</th>
                                    <th>Modify</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('editasset') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>tilga.otilipsta</td>
                                        <td>Tilga Otilipsta</td>
                                        <td>0755509382</td>
                                        <td>tilty@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>HOD</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>bula.boy</td>
                                        <td>bula Boy</td>
                                        <td>0712505670</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Procurement Officer</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>patrick.augustino</td>
                                        <td>Patrick Augustino</td>
                                        <td>0659509332</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>tilga.otilipsta</td>
                                        <td>Tilga Otilipsta</td>
                                        <td>0755509382</td>
                                        <td>tilty@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>HOD</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>bula.boy</td>
                                        <td>bula Boy</td>
                                        <td>0712505670</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Procurement Officer</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>patrick.augustino</td>
                                        <td>Patrick Augustino</td>
                                        <td>0659509332</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>patrick.augustino</td>
                                        <td>Patrick Augustino</td>
                                        <td>0659509332</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>tilga.otilipsta</td>
                                        <td>Tilga Otilipsta</td>
                                        <td>0755509382</td>
                                        <td>tilty@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>HOD</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>bula.boy</td>
                                        <td>bula Boy</td>
                                        <td>0712505670</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Procurement Officer</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                        <tr>
                                        <td>patrick.augustino</td>
                                        <td>Patrick Augustino</td>
                                        <td>0659509332</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                        </tr>
                                        <tr>
                                            <td>tilga.otilipsta</td>
                                            <td>Tilga Otilipsta</td>
                                            <td>0755509382</td>
                                            <td>tilty@gmail.com</td>
                                            <td>active</td>
                                            <td>Information Technology</td>
                                            <td>HOD</td>
                                            <td><a href="{{ Route('edituser') }}">edit</a></td>
                                        </tr>
                                        <tr>
                                            <td>bula.boy</td>
                                            <td>bula Boy</td>
                                            <td>0712505670</td>
                                            <td>patric@gmail.com</td>
                                            <td>active</td>
                                            <td>Procurement Officer</td>
                                            <td>admin</td>
                                            <td><a href="{{ Route('edituser') }}">edit</a></td>
                                        </tr>
                                        <tr>
                                        <td>patrick.augustino</td>
                                        <td>Patrick Augustino</td>
                                        <td>0659509332</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                        </tr>
                                    <tr>
                                        <td>tilga.otilipsta</td>
                                        <td>Tilga Otilipsta</td>
                                        <td>0755509382</td>
                                        <td>tilty@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>HOD</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>bula.boy</td>
                                        <td>bula Boy</td>
                                        <td>0712505670</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Procurement Officer</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->


                </div>
                <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                    <div class="row">
                        <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                            <h3 class="card-title">DataTable with All Users</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                <th>User ID</th>
                                <th>Full Names</th>
                                <th>Phone number</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Department</th>
                                <th>Role</th>
                                <th>Modify</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td>patrick.augustino</td>
                                <td>Patrick Augustino</td>
                                <td>0659509332</td>
                                <td>patric@gmail.com</td>
                                <td>active</td>
                                <td>Information Technology</td>
                                <td>admin</td>
                                <td><a href="{{ Route('editasset') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>tilga.otilipsta</td>
                                    <td>Tilga Otilipsta</td>
                                    <td>0755509382</td>
                                    <td>tilty@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>HOD</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>bula.boy</td>
                                    <td>bula Boy</td>
                                    <td>0712505670</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Procurement Officer</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>tilga.otilipsta</td>
                                    <td>Tilga Otilipsta</td>
                                    <td>0755509382</td>
                                    <td>tilty@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>HOD</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>bula.boy</td>
                                    <td>bula Boy</td>
                                    <td>0712505670</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Procurement Officer</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>tilga.otilipsta</td>
                                    <td>Tilga Otilipsta</td>
                                    <td>0755509382</td>
                                    <td>tilty@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>HOD</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>bula.boy</td>
                                    <td>bula Boy</td>
                                    <td>0712505670</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Procurement Officer</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                    <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>tilga.otilipsta</td>
                                        <td>Tilga Otilipsta</td>
                                        <td>0755509382</td>
                                        <td>tilty@gmail.com</td>
                                        <td>active</td>
                                        <td>Information Technology</td>
                                        <td>HOD</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                        <td>bula.boy</td>
                                        <td>bula Boy</td>
                                        <td>0712505670</td>
                                        <td>patric@gmail.com</td>
                                        <td>active</td>
                                        <td>Procurement Officer</td>
                                        <td>admin</td>
                                        <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                    <tr>
                                    <td>patrick.augustino</td>
                                    <td>Patrick Augustino</td>
                                    <td>0659509332</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                    </tr>
                                <tr>
                                    <td>tilga.otilipsta</td>
                                    <td>Tilga Otilipsta</td>
                                    <td>0755509382</td>
                                    <td>tilty@gmail.com</td>
                                    <td>active</td>
                                    <td>Information Technology</td>
                                    <td>HOD</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                <tr>
                                    <td>bula.boy</td>
                                    <td>bula Boy</td>
                                    <td>0712505670</td>
                                    <td>patric@gmail.com</td>
                                    <td>active</td>
                                    <td>Procurement Officer</td>
                                    <td>admin</td>
                                    <td><a href="{{ Route('edituser') }}">edit</a></td>
                                </tr>
                                </tbody>
                            </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->


                </div>
                <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
                   Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna.
                </div>
                <div class="tab-pane fade" id="custom-tabs-three-settings" role="tabpanel" aria-labelledby="custom-tabs-three-settings-tab">
                   Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis.
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
