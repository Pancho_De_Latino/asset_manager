@extends('layouts.admin')
@section('title', 'Edit Asset')
@section('admin-content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Edit Asset</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ Route('viewasset') }}">View Assets</a></li>
            <li class="breadcrumb-item active">Edit Asset</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
        <div class="row">
            <div class="col-5"></div>
            <div class="col-2">
                <center>
                {!! DNS1D::getBarcodeHTML($asset->barcode, 'I25') !!}
                </center>
            </div>
            <div class="col-4"></div>
        </div>
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-2">
                <center>
                {{ $asset->barcode }}
                </center>
            </div>
            <div class="col-md-5"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <center><img class="img-fluid" src="{{ asset(Storage::url($asset->asset_blueprint->image))}}" /></center>
            </div>
            <div class="col-md-4"></div>
        </div>
        <br />
        <form action="{{ route('updateasset') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <input type="hidden" name="asset_id" value="{{ $asset->id }}">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Serial Number <font color="red">*</font></label>
                            <input type="text" required name="serial_number" value="{{ $asset->serial_number }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Serial number">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">MAC Address</label>
                            <input type="text" name="mac_address" value="{{ $asset->mac_address }}" class="form-control" id="exampleInputEmail1" placeholder="Enter MAC address">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Expire Date <font color="red">*</font></label>
                            <input type="date" required name="expire_date" required value="{{ $asset->expire_date }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Expire Date">
                        </div>
                        <div class="form-group">
                            <label>Assign To <font color="red">*</font></label>
                            <select class="form-control select2bs4" name="assigned_to" style="width: 100%;">
                                <option></option>
                                @foreach ($users as $user)
                                    <option {{ $user->id==$asset->assigned_user ? "selected" : ""}} value="{{ $user->id }}">{{ $user->first_names}} {{ $user->last_name}} | {{ $user->email }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Asset Blueprint <font color="red">*</font></label>
                                <select required class="form-control select2bs4" name="blueprint" style="width: 100%;">
                                    <option></option>
                                    @foreach ($assetblueprints as $assetblueprint)
                                        <option {{ $assetblueprint->blueprint_id==$asset->asset_blueprint->id ? "selected" : ""}} value="{{ $assetblueprint->blueprint_id }}">{{ $assetblueprint->name}} | {{ $assetblueprint->model}} | {{ $assetblueprint->details}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Department <font color="red">*</font></label>
                                <select required name="current_department" class="form-control select2bs4" style="width: 100%;">
                                <option></option>
                                @foreach ($departments as $department)
                                    <option {{ $department->id == $asset->current_department ? "selected" : ""}} value="{{ $department->id }}">{{ $department->department_name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Status <font color="red">*</font></label>
                                <select required name="status" class="form-control select2bs4" style="width: 100%;">
                                <option></option>
                                <option {{ $asset->status == 'active' ? "selected" : ""}} value="active">active</option>
                                <option {{ $asset->status == 'created' ? "selected" : ""}} value="created">created</option>
                                <option {{ $asset->status == 'expired' ? "selected" : ""}} value="expired">expired</option>
                                <option {{ $asset->status == 'disposed' ? "selected" : ""}} value="disposed">disposed</option>
                                <option {{ $asset->status == 'damaged' ? "selected" : ""}} value="damaged">damaged</option>
                                </select>
                            </div>
                            <div class="form-group">
                            &nbsp;
                            <div class="col-md-10 form-check">
                                <input type="checkbox" disabled
                                @if ($asset->asset_blueprint->require_ppm)
                                    checked
                                @endif
                                name="require_ppm" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Requires Preventive Maintanance</label>
                            </div>
                            &nbsp;
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <div class="row">
                @can('edit user')
                    <div class="col-5"></div>
                    <div class="col-2">
                        <center>
                        <button type="submit" class="btn btn-success float-center toastrDefaultSuccess" style="margin-right: 5px;">
                            Update Asset Information
                        </button>
                        </center>
                    </div>
                    <div class="col-5"></div>
                @endcan
            </div>
        </form>
        <br />
        <div class="row">
            <div class="col-12 col-sm-12">
              <div class="card card-primary card-outline card-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                  <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">

                    @if ($asset->asset_blueprint->require_ppm)
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Asset Preventive Maintanance History</a>
                        </li>
                    @endif
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Asset Movement History</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="tab-content" id="custom-tabs-three-tabContent">
                    @if ($asset->asset_blueprint->require_ppm)
                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                        <!-- Main content -->
                        <div class="row">
                                <div class="col-12">

                                <div class="card">
                                    <div class="card-header">
                                    <h3 class="card-title">DataTable with Preventive Maintanance History</h3>
                                    @can('create asset')
                                    <button type="button" class="btn btn-primary float-sm-right" data-toggle="modal" data-target="#modal-default">
                                        <i class="fas fa-plus"></i> Schedule PM Date
                                    </button>
                                    @endcan
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Scheduled at</th>
                                        <th>Status</th>
                                        <th>done_at</th>
                                        <th>done_by</th>
                                        <th>approved_by</th>
                                        <th>PM Document</th>
                                        <th>Modify</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($asset->pm_schedule as $pm_schedules)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{ $pm_schedules->pm_schedule_date }}</td>
                                                    <td>{{ $pm_schedules->status ? 'done':'not done'}}</td>
                                                    <td>{{ $pm_schedules->done_at }}</td>
                                                    <td>{{ $pm_schedules->done_by }}</td>
                                                    <td>{{ $pm_schedules->approved_by }}</td>
                                                    <td>
                                                        <span data-toggle="modal" data-target="#deleteDepartmentModal" data-is_active={{ $pm_schedules->id }}
                                                            class="badge badge-success">view pm document</span>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-success btn-sm"><i class="fa fa-pencil-alt"></i></button>
                                                        <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->


                    </div>
                    @endif
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                        <div class="row">
                            <div class="col-12">

                            <div class="card">
                                <div class="card-header">
                                <h3 class="card-title">DataTable with Asset Movement History</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>Assigned User</th>
                                    <th>Department</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($asset->asset_history as $history)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $history->assigned_user }}</td>
                                            <td>{{ $history->department }}</td>
                                            <td>{{ $history->fromdate }}</td>
                                            <td>{{ $history->todate }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->


                    </div>

                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>
        </div>

    </div><!-- /.container-fluid -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="{{ route('editasset.add.pmschedule') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Create PM Schedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" name="asset_id" value="{{ $asset->id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label>PM Document <font color="red">*</font></label>
                        <select required name="ppm_document" class="form-control select2bs4" style="width: 100%;">
                        <option></option>
                        @foreach ($pm_documents as $pm_document)
                            <option {{ $pm_document->id == $asset->ppm_document ? "selected" : ""}} value="{{ $pm_document->id }}">{{ $pm_document->name}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Date of the Next Preventive Maintanance<font color="red">*</font></label>
                        <input required type="date" name="pm_schedule_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Expire Date">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Schedule</button>
                </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </section>
  <!-- /.content -->
@endsection
