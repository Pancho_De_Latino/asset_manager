@extends('layouts.admin')
@section('title', 'View Assets')
@section('admin-content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>View Assets</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">view assets</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            @include('partial.alert')
        </div>
      </div>
      @canany(['create asset', 'assign asset', 'view asset','edit asset'])
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with All Assets</h3>
              @can('create asset')
              <button type="button" class="btn btn-primary float-sm-right" data-toggle="modal" data-target="#modal-default">
                <i class="fas fa-plus"></i> Add Asset
              </button>
              @endcan
            </div>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Asset Name</th>
                  <th>Model</th>
                  <th>Type</th>
                  <th>Expire Date</th>
                  <th>MAC Address</th>
                  <th>Serial Number</th>
                  <th>Department</th>
                  <th>Modify</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($assets as $asset)
                        <tr>
                            <td>{{ $asset->name }}</td>
                            <td>{{ $asset->model }}</td>
                            <td>{{ $asset->details }}</td>
                            <td>{{ $asset->expire_date }}</td>
                            <td>{{ $asset->mac_address }}</td>
                            <td>{{ $asset->serial_number }}</td>
                            <td>{{ $asset->department_name }}</td>
                            @can('edit asset')
                                <td>
                                  {{-- <a a href="{{ Route('editasset') }}" class="badge bg-danger">edit</a> --}}

                                  <form style="display: inline;" action="{{ route('editasset') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="asset_id" value="{{$asset->assets_id}}" />
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil-alt"></i></button>
                                  </form>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      @endcanany

    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <form action="{{ route('viewasset.add.asset') }}" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Create Asset</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Asset Blueprint <font color="red">*</font></label>
                    <select required class="form-control select2bs4" name="blueprint" style="width: 100%;">
                        <option></option>
                        @foreach ($assetblueprints as $assetblueprint)
                            <option value="{{ $assetblueprint->blueprint_id }}">{{ $assetblueprint->name}} | {{ $assetblueprint->model}} | {{ $assetblueprint->details}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Department <font color="red">*</font></label>
                    <select required class="form-control select2bs4" name="current_department" style="width: 100%;">
                        <option></option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->department_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="mac address">MAC Address</label>
                    <input type="text" class="form-control" name="mac_address" id="exampleInputPassword1" placeholder="Enter asset MAC address">
                </div>
                <div class="form-group">
                    <label for="serial number">Serial Number <font color="red">*</font></label>
                    <input required type="text" name="serial_number" class="form-control" id="exampleInputPassword1" placeholder="Enter asset Serial Number">
                </div>
                <div class="form-group">
                    <label>Expire Date <font color="red">*</font></label>
                    <input required type="date" name="expire_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Expire Date">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create Asset</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection
