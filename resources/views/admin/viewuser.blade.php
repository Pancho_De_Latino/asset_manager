@extends('layouts.admin')
@section('title', 'View Users')
@section('admin-content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>View Users</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">view users</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-12">
            @include('partial.alert')
          </div>
      </div>
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with All Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Full Names</th>
                  <th>Phone</th>
                  <th>Email</th>
                  @can('change user status')
                    <th>Status</th>
                  @endcan
                  <th>Department</th>
                  <th>Role</th>
                  <th>Modify</th>
                </tr>
                </thead>
                <tbody>
                    @canany(['edit user', 'view user'])
                    @foreach ($users as $user)
                       <tr>
                        <td>{{$user->user_name}}</td>
                        <td>{{$user->first_names}} {{$user->last_name}}</td>
                        <td>{{$user->phone_number}}</td>
                        <td>{{$user->email}}</td>
                        @can('change user status')
                        <td>
                            <span data-toggle="modal" data-target="#deleteDepartmentModal" data-is_active={{ $user->user_id }}
                            data-user_id="{{ $user->user_id }}" class="badge {{$user->is_active ? 'badge-success' : 'badge-danger'}}">{{$user->is_active ? 'active' : 'disabled'}}</span>
                        </td>
                        @endcan
                        <td>{{ $user->department_name }}</td>
                        <td>{{$user->name}}</td>
                        <td style="display: inline;">
                          <form style="display: inline;" action="{{ route('edituserfromview') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->user_id}}">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil-alt"></i></button>
                          </form>
                      </td>
                      </tr>
                    @endforeach
                    @endcanany

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->

  <div class="modal fade" id="deleteDepartmentModal">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Change Status</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('viewuser.changestatus') }}" method="post">
            @csrf
            <div class="modal-body">
                <h5>Are you sure you want to Change User Status?</h5>
                <input type="hidden" id="user_id" name="user_id" />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline-light">Yes, Change</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection
@section('page_script')
<script>
    $('#deleteDepartmentModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var user_id = button.data('user_id')
      var modal = $(this)
      modal.find('#user_id').val(user_id)
    })
</script>
@endsection
