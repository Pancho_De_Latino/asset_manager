@extends('layouts.admin')
@section('title', 'Edit User')
@section('admin-content')
@foreach($user as $userdetails)
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ Route('viewuser') }}">View Users</a></li>
            <li class="breadcrumb-item active">Edit User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('partial.alert')
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <center>
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img mb-9"
                                    src="{{ is_null($userdetails->profilepath) ? asset(Storage::url('public/defaultprofile.png')) : asset(Storage::url($userdetails->profilepath)) }}"
                                    alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center">{{$userdetails->user_name }}</h3>


                            <h3 class="profile-username text-center">{{$userdetails->first_names}} {{$userdetails->last_name}}</h3>


                            <h5 class="text-muted text-center">{{$userdetails->department_name}}</h5>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    </center>
                </div>
                <div class="col-md-3"></div>
            </div>
            <form action="{{route('edituserprofile')}}" method="post" enctype="multipart/form-data">
                @csrf
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">User ID</label>
                        <input type="text" value="{{ $userdetails->user_id }}" disabled class="form-control" id="exampleInputEmail1" placeholder="Enter User ID">
                    </div>

                    <input type="hidden" name="user_id" value="{{ $userdetails->user_id }}">

                    <div class="form-group">
                        <label for="exampleInputEmail1">First names</label>
                        <input type="text" required name="first_names" value="{{ $userdetails->first_names }}" class="form-control" id="exampleInputEmail1" placeholder="Enter First name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last name</label>
                        <input type="text" required name="last_name" value="{{ $userdetails->last_name }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Last name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone Number</label>
                        <input type="text" required name="phone_number" required value="{{ $userdetails->phone_number }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone number">
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" required name="email" value="{{ $userdetails->email }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                        </div>
                        <div class="form-group">
                            <label>Department</label>
                            <select required name="current_department" class="form-control select2bs4" style="width: 100%;">
                              <option>--select department--</option>
                              @foreach ($departments as $department)
                                <option {{ $department->id == $userdetails->current_department ? "selected" : ""}} value="{{ $department->id }}">{{ $department->department_name}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select required name="role" class="form-control select2bs4" style="width: 100%;">
                              <option>--select role--</option>
                              @foreach ($roles as $role)
                                <option {{ $role->id == $userdetails->role_id ? "selected" : ""}} value="{{ $role->id }}">{{ $role->name}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Profile Picture </label>
                            <div class="input-group">
                              <div class="custom-file">
                                <input type="file" accept="image/png, image/gif, image/jpeg" name="image" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <div class="row">
            @can('edit user')
                <div class="col-5"></div>
                <div class="col-2">
                    <center>
                    <button type="submit" class="btn btn-success float-center toastrDefaultSuccess" style="margin-right: 5px;">
                        Update User Information
                    </button>
                    </center>
                </div>
                <div class="col-5"></div>
            @endcan
        </div>
        </form>
        <br />
        <form action="{{ route('edituserpermissions')}}" method="post">
            @csrf
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
            <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">{{ $userdetails->name }} | Permissions</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                    <input type="hidden" name="user_id" value="{{ $userdetails->user_id }}">
                    @foreach ($permissions as $permission)
                        <div class="col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" name="{{ $permission->id }}" type="checkbox"
                                @foreach ($userrolepermission as $userpermission)
                                    @if($userpermission->id == $permission->id)
                                        checked
                                    @endif
                                @endforeach
                                @foreach ($userpermissionsviaroles as $userpermissionsviarole)
                                    @if($userpermissionsviarole->id == $permission->id)
                                        disabled
                                    @endif
                                @endforeach
                                >
                                <label class="form-check-label">{{$permission->name}}</label>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <center>
                                <font face="cambria" color="green">Hint**: Disabled Permissions are Role Based Permissions. Hence, Can not be Edited from This Dashboard</font>
                            </center>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
        </div>

        <div class="row">
            @can('edit user')
                <div class="col-5"></div>
                <div class="col-2">
                    <center>
                    <button type="submit" class="btn btn-success float-center" style="margin-right: 5px;">
                        Update User Permissions
                    </button>
                    </center>
                </div>
                <div class="col-5"></div>
            @endcan
        </div>
        </form>
    </section>
</section>
@endforeach
@endsection
@section('page_script')
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script>
$(function() {
    var Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $('.toastrDefaultSuccess').click(function() {
      toastr.success('Successfully Launched')
    });
});
</script>
@endsection
