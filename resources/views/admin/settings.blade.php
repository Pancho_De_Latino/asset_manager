@extends('layouts.admin')
@section('title', 'Settings')
@section('admin-content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Settings</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ Route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Settings</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('partial.alert')
            </div>
        </div>
      <div class="row">
        @canany(['view asset category','add asset category','delete asset category'])
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Asset Category</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->

            <div class="card-body">
                @can('add asset category')
                <form action="{{ route('settings.add.assettype') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <input type="text" value="{{ old('details') }}" class="form-control" name="details" id="exampleInputEmail1" placeholder="Enter Asset Category">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" style="align:right;" name="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
                @endcan
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Asset Category Name</th>
                                    @can('delete asset category')
                                    <th>Action</th>
                                    @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($assetcategories as $assetcategory)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $assetcategory->details }}</td>
                                        @can('delete asset category')
                                            <td><button type="button" data-toggle="modal" data-target="#deleteAssetTypeModal"
                                                data-assetcategory_id="{{ $assetcategory->id }}"
                                                class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button>
                                            </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                    </div>
                </div>
            </div>
          </div>
          <!-- /.card -->

        </div>
        @endcanany

        @canany(['view departments','create department','delete department'])
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Departments</h3>
            </div>
             <div class="card-body">
                @can('create department')
                <form action="{{ route('settings.add.department') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <input type="text" name="department_name" value="{{ old('department_name') }}" class="form-control" id="exampleInputEmail1" placeholder="Enter Department Name">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name="submit" style="align:right;" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
                @endcan
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Department Name</th>
                                    @can('delete department')
                                        <th>Action</th>
                                    @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($departments as $department)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $department->department_name }}</td>
                                        @can('delete department')
                                        <td><button type="button" data-toggle="modal" data-target="#deleteDepartmentModal"
                                            data-department_id="{{ $department->id }}"
                                            class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button>
                                        </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                    </div>
                </div>
             </div>
          </div>
          <!-- /.card -->
        </div>
        @endcanany

        @canany(['create role','view role permissions','edit role permissions','delete role'])
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">
                  Roles and Permissions
              </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
             <div class="card-body">
                @can('create role')
                <form action="{{ route('settings.add.role') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <input type="text" required value="{{ old('role_name') }}" class="form-control" name="role_name" id="exampleInputEmail1" placeholder="Enter New Role Name">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" style="align:right;" name="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
                @endcan

                @canany(['view role permissions','edit role permissions','delete role'])
                <form action="{{route('settings.edit.rolepermission')}}" method="post">
                    @csrf
                @foreach ($roles as $role)
                <div class="row">
                    <div class="col-md-12">
                        <!-- general form elements disabled -->

                    <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">{{ $role->name}} | Permissions</h3>
                          @can('delete role')
                            <button type="button" data-toggle="modal" data-target="#deleteRoleModal" data-role_id="{{$role->id}}"
                            class="btn btn-danger btn-sm float-right"><i class="fa fa-trash-alt"></i></button>
                          @endcan
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                            @foreach ($permissions as $permission)
                                <div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" name="{{ $role->id }}-{{$permission->id}}" type="checkbox"
                                        @if ($role->hasPermissionTo($permission))
                                        checked
                                        @endif
                                        >
                                        <label class="form-check-label">{{$permission->name}}</label>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                </div>
                @endforeach

                @endcanany
                @can('edit role permissions')
                <div class="form-group">
                    <center>
                        <button type="submit" name="submit" style="align:right;" class="btn btn-primary">Edit Role Permissions</button>
                    </center>
                </div>
                @endcan
                </form>
             </div>
          </div>
          <!-- /.card -->
        </div>
        @endcanany

        @canany(['create ppm checklist','view role permissions','edit role permissions','delete role'])
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">
                  Preventive Maintanance Documents
              </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
             <div class="card-body">
                @can('create ppm checklist')
                <form action="{{ route('settings.add.document') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <input type="text" required value="{{ old('document_name') }}" class="form-control" name="document_name" id="exampleInputEmail1" placeholder="Enter New Document Name">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" style="align:right;" name="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
                @endcan

                @canany(['view role permissions','edit role permissions','delete role'])
                @foreach ($documents as $document)
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-secondary">
                            <div class="card-header">
                            <h3 class="card-title">{{ $document->name}}</h3>
                            @can('delete role')
                                <button type="button" data-toggle="modal" data-target="#deleteRoleModal" data-document_id="{{$document->id}}"
                                class="btn btn-danger btn-sm float-right"><i class="fa fa-trash-alt"></i></button>
                            @endcan
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @can('create ppm checklist')
                                <form action="{{ route('settings.add.question') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="hidden" required value="{{ $document->id }}" class="form-control" name="document_id">
                                                <input type="text" required value="{{ old('document_question') }}" class="form-control" name="document_question" id="exampleInputEmail1" placeholder="Enter New Name Checklist Name">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" style="align:right;" name="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                                @endcan
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Checklist Name</th>
                                                <th>Modify</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($document->pm_questions as $pmquestion)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$pmquestion->name}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-success btn-sm"><i class="fa fa-pencil-alt"></i></button>
                                                        <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                @endforeach
                @endcanany
             </div>
          </div>
          <!-- /.card -->
        </div>
        @endcanany

        @canany(['view asset blueprint','add asset blueprint','delete asset blueprint'])
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Asset Blueprint

              </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
             <div class="card-body">
                @can('add asset blueprint')
                <form action="{{ route('settings.add.assetblueprint') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name <font color="red">*</font></label>
                        <input type="text" name="name" value="{{ old('name')}}" class="form-control" id="exampleInputEmail1" placeholder="Enter asset name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Model <font color="red">*</font></label>
                        <input type="text" name="model" value="{{ old('model')}}" class="form-control" id="exampleInputPassword1" placeholder="Enter asset model">
                    </div>
                    <div class="form-group">
                        <label>Asset Type <font color="red">*</font></label>
                        <select class="form-control select2bs4" name="type" style="width: 100%;">
                            <option></option>
                            @foreach ($assetcategories as $assetcategory)
                                <option {{ old('type') == $assetcategory->id ? "selected" : ""}} value="{{ $assetcategory->id }}">{{ $assetcategory->details}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Asset Image <font color="red">*</font></label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 form-check">
                        <input type="checkbox" name="require_ppm" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Requires Preventive Maintanance</label>
                    </div>
                    <div class="form-group">
                        <center>
                            <button type="submit" name="submit" style="align:right;" class="btn btn-primary">Add</button>
                        </center>
                    </div>
                    </div>
                </form>
                @endcan
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    @can('delete asset blueprint')
                                        <th>Action</th>
                                    @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($assetblueprints as $assetblueprint)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $assetblueprint->name }}</td>
                                        <td>{{ $assetblueprint->model }}</td>
                                        <td>{{ $assetblueprint->details }}</td>
                                        @can('delete asset blueprint')
                                            <td><button type="button" data-toggle="modal" data-target="#deleteAssetBlueprintModal"
                                                data-assetblueprintid="{{ $assetblueprint->blueprint_id }}"
                                                class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></button>
                                            </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                    </div>
                </div>
             </div>
          </div>
          <!-- /.card -->
        </div>
        @endcanany


      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  <div class="modal fade" id="deleteAssetBlueprintModal">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Deleting Asset Blueprint</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('settings.delete.assetblueprint') }}" method="post">
            @csrf
            <div class="modal-body">
                <h5>Are you sure you want to delete this blueprint?</h5>
                <input type="hidden" id="blueprint_id" name="blueprint_id" />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline-light">Yes, Delete</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="deleteRoleModal">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Deleting Role</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('settings.delete.role') }}" method="post">
            @csrf
            <div class="modal-body">
                <h5>Are you sure you want to delete this Role?</h5>
                <input type="hidden" id="role_id" name="role_id" />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline-light">Yes, Delete</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="deleteDepartmentModal">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Deleting Department</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('settings.delete.department') }}" method="post">
            @csrf
            <div class="modal-body">
                <h5>Are you sure you want to delete this Department?</h5>
                <input type="hidden" id="department_id" name="department_id" />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline-light">Yes, Delete</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="deleteAssetTypeModal">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Deleting Asset Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('settings.delete.assettype') }}" method="post">
            @csrf
            <div class="modal-body">
                <h5>Are you sure you want to delete this Department?</h5>
                <input type="hidden" id="assetcategory_id" name="assetcategory_id" />
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-outline-light">Yes, Delete</button>
            </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection
@section('page_script')
<script>
    $('#deleteAssetBlueprintModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var blueprint_id = button.data('assetblueprintid')
      var modal = $(this)
      modal.find('#blueprint_id').val(blueprint_id)
    })
</script>
<script>
    $('#deleteDepartmentModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var department_id = button.data('department_id')
      var modal = $(this)
      modal.find('#department_id').val(department_id)
    })
</script>
<script>
    $('#deleteAssetTypeModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var assetcategory_id = button.data('assetcategory_id')
      var modal = $(this)
      modal.find('#assetcategory_id').val(assetcategory_id)
    })
</script>
<script>
    $('#deleteRoleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var role_id = button.data('role_id')
      var modal = $(this)
      modal.find('#role_id').val(role_id)
    })
</script>
@endsection
