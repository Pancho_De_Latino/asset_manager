@if (Session::has('success'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    {{ Session::get('success') }}
</div>
@endif

@if (Session::has('barcode'))
  <div class="alert alert-default alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fa fa-check"></i> Asset Created Successfully</h5>
    <div>
        <center>
        <?php $barcode = Session::get('barcode') ?>
        {!! DNS1D::getBarcodeHTML(Session::get('barcode'), 'I25') !!}
        <form action="{{ route('barcode') }}" method="post">
            @csrf
            <button rel="noopener" name="barcode" target="_blank" value="{{ Session::get('barcode') }}" class="btn btn-success"><i class="fas fa-print"></i> Print</button>
        </form>

        </center>
    </div>
</div>
@endif

@if (Session::has('error'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Info!</h4>
    {{ Session::get('error') }}
</div>
@endif

@if (count($errors) > 0)

  <div class=" alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Failed!</h4>
    @foreach ($errors->all() as $error)
        <ul>
            <li>{{ $error }}</li>
        </ul>
    @endforeach

</div>
@endif
